cmake_minimum_required(VERSION 3.14)

include("${CMAKE_SOURCE_DIR}/cmakepp/cmakepp.cmake")

if(NOT VERSION)
	set(VERSION "0.0.0")
endif()

# Find/make a SemVer-compatible version string from available information
# The Makefile will attempt to create release/wip version strings. Used as-is.
# Otherwise, we should have a human-readable string, so compose from it.
# Then, if the repo is dirty, we append an "!" to make sure it's recorded.
find_package(Git REQUIRED QUIET)
string(REGEX MATCH "^(v?[0-9]+(\\.[0-9]+)?(\\.[0-9]+)?(\\.[0-9]+)?(-[a-zA-Z0-9_]+)?(\\+[a-zA-Z0-9_]+)?!?)$" VALID_SEMVER ${VERSION})
if(VALID_SEMVER)
	semver_constraint_evaluate(">0.0.0" "${VERSION}")
	ans(VALID_VERSION)
else()
	set(VALID_VERSION 0)
endif()
if(VALID_VERSION)
	message(STATUS "Valid SemVer found; using it.")
	set(FULL_PROJECT_VERSION ${VERSION})
	string_split_at_first(SEMVER prerelease_and_metadata "${VERSION}" "-")
	message(STATUS "SEMVER: ${SEMVER}")
else()
	message(STATUS "This is a Development build. Adding git checkout information to version 0.0.0")
	execute_process(
		COMMAND ${GIT_EXECUTABLE} -C "${PROJECT_SOURCE_DIR}" rev-parse --abbrev-ref HEAD
		OUTPUT_STRIP_TRAILING_WHITESPACE
		OUTPUT_VARIABLE git_branch)
	execute_process(
		COMMAND ${GIT_EXECUTABLE} -C "${PROJECT_SOURCE_DIR}" describe --match ForceNone --abbrev=10 --always
		OUTPUT_STRIP_TRAILING_WHITESPACE
		OUTPUT_VARIABLE git_revision)

	set(SEMVER "0.0.0")
	set(FULL_PROJECT_VERSION "${SEMVER}-${git_branch}+${git_revision}")
	#message(STATUS ${FULL_PROJECT_VERSION})
endif()

# Check if repo is dirty. Append a ! to the end of the project version, if so.
execute_process(
	COMMAND ${GIT_EXECUTABLE} -C "${PROJECT_SOURCE_DIR}"  update-index --really-refresh)
execute_process(
	COMMAND ${GIT_EXECUTABLE} -C "${PROJECT_SOURCE_DIR}" diff-index --quiet HEAD
	RESULT_VARIABLE git_dirty)
if (NOT git_dirty EQUAL 0)
	set(FULL_PROJECT_VERSION "${FULL_PROJECT_VERSION}!")
endif()
message(STATUS "Full project version: ${FULL_PROJECT_VERSION}")

project(uttu VERSION ${SEMVER} LANGUAGES CXX)
set(PROJECT_VERSION ${FULL_PROJECT_VERSION})
message(STATUS "Project version: ${PROJECT_VERSION}")
set(APPLICATION_SUMMARY "Uttu HTTP Server")
set(CMAKE_PROJECT_DESCRIPTION "Static HTTP Server with modern protocol support")
set(CMAKE_PROJECT_HOMEPAGE_URL "https://uttu.app/")
add_compile_definitions(APPLICATION_NAME="${APPLICATION_SUMMARY}")
add_compile_definitions(APPLICATION_VERSION="${PROJECT_VERSION}")
add_compile_definitions(PROJECT_NAME="${CMAKE_PROJECT_NAME}")

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core HttpServer LinguistTools)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core HttpServer LinguistTools)
qt_standard_project_setup()

set(TS_FILES uttu_es_419.ts)

qt_add_executable(${CMAKE_PROJECT_NAME}
  main.cpp
  ${TS_FILES}
)
target_link_libraries(uttu PRIVATE Qt${QT_VERSION_MAJOR}::Core)
target_link_libraries(uttu PRIVATE Qt${QT_VERSION_MAJOR}::HttpServer)

if(COMMAND qt_create_translation)
    qt_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
else()
    qt5_create_translation(QM_FILES ${CMAKE_SOURCE_DIR} ${TS_FILES})
endif()

# Ensure Git ignores any build directories
if(NOT PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
    file(GENERATE OUTPUT .gitignore CONTENT "*")
endif()

# Apple's tools don't work how lots of people expect. This corrects that.
if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    set(CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> Scr <TARGET> <LINK_FLAGS> <OBJECTS>")
    set(CMAKE_CXX_ARCHIVE_CREATE "<CMAKE_AR> Scr <TARGET> <LINK_FLAGS> <OBJECTS>")
    set(CMAKE_C_ARCHIVE_FINISH "<CMAKE_RANLIB> -no_warning_for_no_symbols -c <TARGET>")
    set(CMAKE_CXX_ARCHIVE_FINISH "<CMAKE_RANLIB> -no_warning_for_no_symbols -c <TARGET>")
endif()

include(GNUInstallDirs)
install(TARGETS uttu
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# FIXME: Need to add some smarts for the generator
set(CPACK_GENERATOR "RPM")
set(CPACK_PACKAGE_NAME ${CMAKE_PROJECT_NAME})
set(CPACK_PACKAGE_VERSION ${CMAKE_PROJECT_VERSION})
set(CPACK_RPM_PACKAGE_RELEASE "1")
set(CPACK_RPM_PACKAGE_VENDOR "The Uttu Project")
# Package groups are de-suggested by at least Red Hat and Suse these days
# However, this is the traditional group such a project would be under
# PR if other distros or non-Linux RPM-based OSes suggest a better group
set(CPACK_RPM_PACKAGE_GROUP "System Environment/Daemons")
set(CPACK_RPM_PACKAGE_DESCRIPTION ${CMAKE_PROJECT_DESCRIPTION})
set(CPACK_RPM_PACKAGE_DESCRIPTION_SUMMARY ${APPLICATION_SUMMARY})
set(CPACK_RPM_PACKAGE_LICENSE "GPL-3.0-only WITH Qt-GPL-exception-1.0")
set(CPACK_RPM_FILE_NAME "RPM-DEFAULT")
include(CPack)
include(InstallRequiredSystemLibraries)
