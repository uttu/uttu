= Files from CMake\++

https://github.com/AnotherFoxGuy/cmakepp[CMake\++] is a suite of CMake recipes released under the MIT license.

This is a slightly-modified copy of release `2022.09`. Changes as follows:

* Updated the VERSION argument <min> value to `3.5`.

