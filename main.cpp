#include <QCoreApplication>
#include <QHttpServer>
#include <QHttpServerRequest>
#include <QHttpServerResponse>
#include <QLocale>
#include <QTranslator>
#include <QThreadPool>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "uttu_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }

    qInfo().noquote() << "Threads available:" << QThreadPool::globalInstance()->maxThreadCount();

    auto m_httpServer = new QHttpServer();
    auto const publicAddr = "0.0.0.0";
    auto const runPort = m_httpServer->listen(QHostAddress::Any, 8080);
    m_httpServer->route("/", []() {
        return QString("%1 %2").arg(APPLICATION_NAME, APPLICATION_VERSION);
    });
    m_httpServer->afterRequest([](const QHttpServerRequest &request, QHttpServerResponse &&response) {
      qDebug().noquote() << QString("HTTP request:")  << request.remoteAddress().toString() << "<-" << request.url().toDisplayString();
      response.setHeader("Server", QString("%1/%2").arg(APPLICATION_NAME, APPLICATION_VERSION).toUtf8());
      return std::move(response);
    });
    if (!runPort) {
        qFatal() << "Cannot continue: HTTP server failed to bind to port" << 8080;
    } else {
        qInfo().noquote() << QString("Uttu is listening to http://%1:8080").arg(publicAddr);
    }
    return a.exec();
}
